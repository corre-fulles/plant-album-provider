# Importing Libraries
import sys
sys.dont_write_bytecode = True
from directory_tree import display_tree

# Main Method
if __name__ == '__main__':
    display_tree(max_depth=3)