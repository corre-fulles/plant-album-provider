import click
from dependency_injector.wiring import inject, Provide

from app.domain.errors import ErrorDependencia
from app.infrastructure.framework import AplicacioWebSelector, seleccionar_aplicacio_web
from app.infrastructure.persistencia import AlbumRepositoryContainer
from app.infrastructure.serveis import ImportadorAlbumsSelector, seleccionar_importador
from app.middleware.persistencia.repository import AbstractDBAlbumRepository
from app.use_case import ImportarAlbums, AfegirAlbums, PeticioAfegirAlbums


# def crear_driver():
#
#
#     is_local = strtobool(os.environ.get('DB_MONGODB_IS_LOCAL', "false"))
#     bd_host = os.environ.get('DB_MONGODB_HOST')
#     print(bd_host)
#     bd_port = os.environ.get('DB_MONGODB_PORT')
#     if bd_host and bd_port:
#         driverBD = MongoDBDriver(
#             host=bd_host,
#             port=bd_port,
#             tls=not is_local,
#             tlsCertificateKeyFile=ruta_certificat_remot() if not is_local else None
#
#         )
#         return driverBD
#     else:
#         raise ValueError("Cal especificar una base de dades")
#
# database_driver = crear_driver()
# repositori_albums = MongoDBAlbumRepository(database_driver)


@click.group()
def cli():
    """
    This is a command line interface for the app
    """
    pass


@cli.command()
@inject
def check_runmode(config=Provide[AlbumRepositoryContainer.config]):
    click.echo("Variables d'entorn ")
    click.echo("Basic")
    click.echo(config['app'])
    click.echo("Bades de dades")
    click.echo(config['databases'])
    click.echo("APIs")
    click.echo(config['apis'])
    click.echo("Certificats")
    click.echo(config['certificats'])


@cli.command()
@click.option('--port', type=int, default=8050,
help='Port on l\'aplicació escolta les peticions. Per defecte: 8050')
@click.option('--host', type=str, default='0.0.0.0',
help='IP local on desplegar l\'aplicació. Per defecte: 0.0.0.0')
@click.option('--agent', type=str, default='flask',
help='Determina el framework. Per defecte: flask')
@click.option('--use-ssl', is_flag=True,
help='Habilita l\'ús de SSL a l\'aplicació web (https). Per defecte: False')
@inject
def serve(port, host, agent, use_ssl,
          selector_aplicacio_web: AplicacioWebSelector = Provide[AplicacioWebSelector],
          ):
    """
    Inicia l'aplicació web
    """
    aplicacio = seleccionar_aplicacio_web(agent, selector_aplicacio_web)
    aplicacio.run(host, port, use_ssl)

    #appSelector = AplicacioWebSelector()
    #appSelector.config.app.from_dict({'agent': agent, 'entorn': environment_mode})

    # aplicacio = Flask('albums')
    # aplicacio.json.ensure_ascii = False
    # controller = FlaskAlbumController(repositori_albums)
    #
    # aplicacio.add_url_rule(
    #     "/albums/", methods=['GET'], view_func=controller.llista_albums)
    # debug = environment_mode != 'prod'
    #
    # if debug:
    #     aplicacio.run(
    #         debug=True, ssl_context='adhoc' if use_ssl else None, host=host, port=port)
    # else:
    #     from waitress import serve
    #     from werkzeug.middleware.proxy_fix import ProxyFix
    #     aplicacio.wsgi_app = ProxyFix(
    #         aplicacio.wsgi_app, x_for=1, x_proto=1, x_host=1)
    #
    #     serve(aplicacio, url_scheme='https' if use_ssl else None,
    #           host=host, port=port)


@cli.command()
@click.argument('pais', type=str)
@click.argument('ciutat', type=str)
@inject
def importar_albums(
        pais: str,
        ciutat: str,
        repositori_albums: AbstractDBAlbumRepository = Provide[AlbumRepositoryContainer.repositori],
        importador_selector: ImportadorAlbumsSelector = Provide[ImportadorAlbumsSelector]
):
    try:
        cas_us_importar = ImportarAlbums(seleccionar_importador(pais,ciutat, importador_selector))
        resposta = cas_us_importar.executar()
        click.echo(f"S'ha importat {len(resposta.albums)} Albums")
        cas_us_afegir = AfegirAlbums(repositori_albums)
        cas_us_afegir.executar(PeticioAfegirAlbums(
            pais=pais, ciutat=ciutat, albums=resposta.albums))
        click.echo("Albums afegits a la base de dades")
    except ErrorDependencia as importadorNoExistent:
        click.echo(f"Error de Dependència: {importadorNoExistent}",err=True)
    except Exception as errorDesconegut:
        click.echo(errorDesconegut, err=True)


if __name__ == "__main__":
    dbcontainer = AlbumRepositoryContainer()
    importadorContainer = ImportadorAlbumsSelector()
    aplicacio_web_container = AplicacioWebSelector()
    dbcontainer.wire(modules=[__name__])
    importadorContainer.wire(modules=[__name__])
    aplicacio_web_container.wire(modules=[__name__])

    cli()
