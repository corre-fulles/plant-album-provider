from abc import ABC, abstractmethod
from typing import List, Any


from app.domain.interfaces import IAlbumRepository
from app.use_case import RespostaObtenirAlbumsDeCiutat


class AbstractAlbumController(ABC):

    def __init__(self, repositori_albums: IAlbumRepository):
        self.repositori = repositori_albums

    @staticmethod
    @abstractmethod
    def _serialitza_contingut_resposta(albums: RespostaObtenirAlbumsDeCiutat) -> List[Any]:
        pass

    @abstractmethod
    def llista_albums(self, *args, **kwargs):
        raise NotImplementedError
