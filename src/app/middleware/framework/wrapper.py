from abc import ABC, abstractmethod
from typing import Any


class AbstractFrameworkAppWrapper(ABC):

    def __init__(self, app: Any):
        self.app = app

    @abstractmethod
    def run(self, *args, **kwargs):
        pass


class FrameworkRouteManager(ABC):
    @abstractmethod
    def afegir_ruta(self, url: str, metodes: list, handler, *args, **kwargs):
        pass
