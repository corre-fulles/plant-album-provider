from abc import ABC, abstractmethod

from app.middleware.controller import AbstractAlbumController



class AplicacioWeb(ABC):

    def __init__(self, controller: AbstractAlbumController, entorn:str):
        self.controller = controller
        self.entorn = entorn

    @abstractmethod
    def _inicialitzar_rutes(self):
        raise NotImplementedError

    @abstractmethod
    def run(self, host:str, port:int, ssl:bool):
        raise NotImplementedError
