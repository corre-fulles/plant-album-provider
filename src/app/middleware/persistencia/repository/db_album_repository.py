from abc import abstractmethod

from app.domain.interfaces import IAlbumRepository
from app.middleware.persistencia.database import DBDriver


class AbstractDBAlbumRepository(IAlbumRepository):

    @abstractmethod
    def __init__(self, driver: DBDriver):
        raise NotImplementedError
