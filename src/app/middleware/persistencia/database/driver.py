from abc import ABC, abstractmethod


class DBDriver(ABC):

    @abstractmethod
    def connectar(self, *args, **kwargs):
        pass
