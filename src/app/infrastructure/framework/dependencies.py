from dependency_injector import providers, containers, errors

from app.config import AppConfigContainer
from app.infrastructure.persistencia import AlbumRepositoryContainer

from app.middleware.framework.aplicacio_web import AplicacioWeb
from .falcon.aplicacio import AplicacioFalcon, FalconAlbumController
from .flask import FlaskAlbumController, AplicacioFlask
from app.domain.errors import ErrorDependencia


class AplicacioWebSelector(containers.DeclarativeContainer):
    config = AlbumRepositoryContainer.config

    app = providers.Selector(
        config.web.agent,
        flask=providers.Singleton(
            AplicacioFlask,
            controlador=providers.Factory(
                FlaskAlbumController,
                AlbumRepositoryContainer.repositori
            ),
            entorn=config.app.environment,
            import_name='albums'
        ),
        falcon=providers.Singleton(
            AplicacioFalcon,
            controlador=providers.Factory(
                FalconAlbumController,
                AlbumRepositoryContainer.repositori
            ),
            entorn=config.app.environment
        )
    )


def seleccionar_aplicacio_web(agent_web:str, web_selector:AplicacioWebSelector) -> AplicacioWeb:
    try:
        web_selector.config.web.agent.from_value(agent_web)
        return web_selector.app()
    except errors.NoSuchProviderError:
        raise ErrorDependencia(f"No existeix l'aplicació web de nom {agent_web}")

