from typing import List, Dict

import falcon
from falcon.request import Request as FalconRequest
from falcon.response import Response as FalconResponse

from app.domain.entities import Album
from app.middleware.controller import AbstractAlbumController
from app.use_case import ObtenirAlbumsDeCiutat, PeticioObtenirAlbumsDeCiutat, RespostaObtenirAlbumsDeCiutat


class FalconAlbumController(AbstractAlbumController):

    @staticmethod
    def _serialitza_contingut_resposta(resposta: RespostaObtenirAlbumsDeCiutat) -> List[Dict]:
        return list(map(lambda a: a.toJSON(), resposta.albums))

    def llista_albums(self, req: FalconRequest):
        resposta = {}
        codi_resposta = 500
        ciutat = req.get_param('ciutat', default=None)
        if ciutat:
            pais = req.get_param('codi_pais', default='spain')
            try:
                obtenir_albums = ObtenirAlbumsDeCiutat(self.repositori)
                resposta_albums = obtenir_albums.executar(
                    PeticioObtenirAlbumsDeCiutat(pais, ciutat))
                resposta.update(
                    status='OK', albums=self._serialitza_contingut_resposta(resposta_albums)
                )
                codi_resposta = 200
            except Exception as e:
                resposta.update(status='KO', error=str(e))
        else:
            resposta.update(status='KO', error="Missing parameter 'ciutat'")
            codi_resposta = 400

        return resposta, codi_resposta

    def on_get_llista_albums(self, req: FalconRequest, resp: FalconResponse):
        try:
            resultat, codi = self.llista_albums(req)
            resp.media = resultat
            resp.set_header('Access-Control-Allow-Origin', '*')
            resp.content_type = 'application/json'
            resp.status = falcon.HTTP_200 if codi == 200 else falcon.HTTP_400 if codi == 400 else falcon.HTTP_500
        except Exception as err:
            resp.status = falcon.HTTP_500
            resp.content_type = 'application/json'
            resp.media = {'status': 'KO', 'error': str(err)}
