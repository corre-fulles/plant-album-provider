import falcon
from werkzeug import run_simple

from app.middleware.framework.aplicacio_web import AplicacioWeb
from .controller import FalconAlbumController


class AplicacioFalcon(AplicacioWeb):
    def __init__(self, controlador: FalconAlbumController, entorn: str, *args, **kwargs):
        super().__init__(controlador, entorn)
        self.app = falcon.App(*args, **kwargs)
        self._inicialitzar_rutes()

    def _inicialitzar_rutes(self):
        self.app.add_route("/albums/", self.controller, suffix='llista_albums')

    def run(self, host: str, port: int, ssl: bool, **kwargs):
        if self.entorn == 'dev':
            run_simple(
                application=self.app,
                hostname=host,
                port=port,
                use_reloader=True,
                use_debugger=True,
                ssl_context='adhoc' if ssl else None,
                **kwargs)
        else:
            from werkzeug.middleware.proxy_fix import ProxyFix
            from waitress import serve
            serve(
                ProxyFix(self.app, x_for=1, x_proto=1, x_host=1),
                url_scheme='https' if ssl else 'http',
                host=host,
                port=port,
                **kwargs)
