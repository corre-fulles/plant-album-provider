from flask import Flask


from app.middleware.framework.aplicacio_web import AplicacioWeb

from .controller import FlaskAlbumController


class AplicacioFlask(AplicacioWeb):
    def __init__(self, controlador: FlaskAlbumController, entorn: str, *args, **kwargs):
        super().__init__(controlador, entorn)
        self.app = Flask(*args, **kwargs)
        self._inicialitzar_rutes()

    def _inicialitzar_rutes(self):
        self.app.add_url_rule("/albums/", methods=['GET'], view_func=self.controller.llista_albums)

    def run(self, host:str, port:int, ssl:bool, **kwargs):
        if self.entorn == 'dev':
            self.app.run(
                debug=True, ssl_context='adhoc' if ssl else None, host=host, port=port, **kwargs)
        else:
            from waitress import serve
            from werkzeug.middleware.proxy_fix import ProxyFix
            self.app.wsgi_app = ProxyFix(
                self.app.wsgi_app, x_for=1, x_proto=1, x_host=1)
            serve(self.app, url_scheme='https' if ssl else 'http', host=host, port=port)