from typing import List, Dict

from flask import request, jsonify

from app.domain.entities import Album
from app.middleware.controller import AbstractAlbumController
from app.use_case import ObtenirAlbumsDeCiutat, PeticioObtenirAlbumsDeCiutat, RespostaObtenirAlbumsDeCiutat


class FlaskAlbumController(AbstractAlbumController):

    @staticmethod
    def _serialitza_contingut_resposta(resposta: RespostaObtenirAlbumsDeCiutat) -> List[Dict]:
        return list(map(lambda a: a.toJSON(), resposta.albums))

    def llista_albums(self):
        resposta = {}
        codi_resposta = 500
        ciutat = request.args.get('ciutat', None)
        if ciutat:
            pais = request.args.get('codi_pais', 'spain')
            try:
                obtenir_albums = ObtenirAlbumsDeCiutat(self.repositori)
                resposta_albums = obtenir_albums.executar(
                    PeticioObtenirAlbumsDeCiutat(pais, ciutat))
                resposta.update(
                    status='OK', albums=self._serialitza_contingut_resposta(resposta_albums))
                codi_resposta = 200
            except Exception as e:
                resposta.update(status='KO', error=str(e))
        else:
            resposta.update(status='KO', error="El paràmetre 'ciutat' és obligatori")
            codi_resposta = 400

        resposta_flask = jsonify(**resposta)
        resposta_flask.headers.add('Access-Control-Allow-Origin', '*')

        return resposta_flask, codi_resposta
