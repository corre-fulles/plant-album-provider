class ServeiError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        
        
class ServeiApiError(ServeiError):
    def __init__(self, message: str):
        super().__init__(message)
        
        
class ServeiImportadorError(ServeiError):
    def __init__(self, message: str):
        super().__init__(message)