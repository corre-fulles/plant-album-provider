import re
from typing import List

from app.domain.entities import Album, EspaiVerd, Planta, NomComu, EspaiVerdDTO, PlantaDTO
from app.domain.interfaces import ServeiImportadorAlbums
from app.utils import BaseLogger
from shapely.geometry.base import BaseGeometry


class ServeiImportadorAlbumsBarcelona(ServeiImportadorAlbums):
    """Implementació de ImportadorAlbums per Barcelona

    Atributs:
        REGEX_NOM_ESPAI_VERD_PLANTA_DTO: expressio regular pel nom d'espai verd d'una PlantaDTO
        REGEX_NOM_ESPAI_VERD_ESPAI_VERD_DT: expressio regular pel nom d'un EspaiVerdDTO
    Returns:
        _type_: _description_
    """

    REGEX_NOM_ESPAI_VERD_PLANTA_DTO = r"^(?P<nom_curt>[\w\s]+), (?:Parc|Jardi?ns).*$"
    REGEX_NOM_ESPAI_VERD_ESPAI_VERD_DTO = r"(?:Parc|Jardi?ns) (?:de|d')?(?:ls\s|\s(?:l\'|la\s)|l\s)?(?P<nom_curt>[\w\s]+)"

    def _crear_albums(self, espais: List[EspaiVerdDTO], plantes: List[PlantaDTO]):
        BaseLogger.info(f"#espais: {len(espais)} ; #plantes: {len(plantes)}")

        albums = []


        def nom_reduit_espai(nom_espai: str):
            captures = re.match(self.REGEX_NOM_ESPAI_VERD_ESPAI_VERD_DTO,
                                nom_espai, re.IGNORECASE)
            if captures:
                return captures.group('nom_curt').lower().strip()
            else:
                return nom_espai.lower().strip()

        def nom_reduit_planta(nom_espai_planta: str):
            captures = re.match(self.REGEX_NOM_ESPAI_VERD_PLANTA_DTO, nom_espai_planta, re.IGNORECASE)
            if captures:
                return captures.group('nom_curt').lower().strip()
            else:
                return nom_espai_planta.lower().strip()

        def espai_conte_planta(espai: EspaiVerdDTO, planta: PlantaDTO) -> bool:
            if hasattr(planta, "dades_posicio") and planta.dades_posicio:
                nom_espai_planta = nom_reduit_planta(
                    planta.dades_posicio.get('espai_verd', ""))  # type: ignore
                if mapa_espais.get(nom_espai_planta, None) == espai.nom:
                    return True
                if hasattr(espai, 'dades_posicio') and espai.dades_posicio:
                    if 'coordenades' in espai.dades_posicio \
                            and isinstance(espai.dades_posicio['coordenades'], BaseGeometry):
                        if espai.dades_posicio['coordenades'].contains(planta.dades_posicio['coordenades']):
                            return True
            return False

        mapa_espais = {nom_reduit_espai(
            espai.nom): espai.nom for espai in espais}

        index_plantes_pendents = list(range(len(plantes)))
        for espai in espais:
            BaseLogger.info(f"[Album: {espai.nom}] -> INICI")
            mapa_plantes_espai = {}
            plantes_assignades = []
            index_plantes_no_assignades = []
            for index_planta in index_plantes_pendents:
                clau_planta = plantes[index_planta].nom_cientific.lower()
                te_planta = mapa_plantes_espai.get(clau_planta, False)

                if not te_planta and espai_conte_planta(espai, plantes[index_planta]):
                    plantes_assignades.append(plantes[index_planta])
                    BaseLogger.info(
                        f"[Album: {espai.nom}] -> PLANTA '{plantes[index_planta].nom_cientific}' -> ASSIGNADA")
                    mapa_plantes_espai[clau_planta] = True
                else:
                    index_plantes_no_assignades.append(index_planta)

            index_plantes_pendents = index_plantes_no_assignades

            album = Album(EspaiVerd(espai.nom, espai.districte), list(map(lambda x: Planta(
                nom_cientific=x.nom_cientific, nom_comu=NomComu(x.nom_comu)
            ), plantes_assignades)), acreditacions={
                'espais verds': self._api_espais.acreditacio,
                'plantes': self._api_plantes.acreditacio

            })
            albums.append(album)
            BaseLogger.info(f"[Album: {espai.nom}] -> GENERAT CORRECTAMENT ({len(plantes_assignades)} entrades)")
        return albums
