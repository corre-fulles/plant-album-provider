from typing import Dict

from app.domain.interfaces import ServeiImportadorAlbums


class ImportadorFactory:
    _instancia = None

    def __new__(cls, *args, **kwargs):
        if not cls._instancia:
            cls._instancia = super().__new__(cls, *args, **kwargs)
        return cls._instancia

    def __init__(self) -> None:
        self.importadors = {}

    def afegir_servei(self, pais: str, ciutat: str, servei: ServeiImportadorAlbums):
        if pais not in self.importadors:
            self.importadors[pais] = {}
        self.importadors[pais][ciutat] = servei

    def afegir_serveis(self, importadors: Dict[str, Dict[str, ServeiImportadorAlbums]]):
        self.importadors = importadors

    def crear_servei(self, pais, ciutat) -> ServeiImportadorAlbums:
        return self.importadors[pais][ciutat]
