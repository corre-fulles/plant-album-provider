from typing import List, Dict

import requests
import shapely.wkt
from pypika import Query, Table, functions as fn

from app.domain.entities import Acreditacio
from app.domain.entities import PlantaDTO
from app.domain.interfaces import IServeiApiPlantes
from app.utils import treure_espais


class OpenDataBcnServeiApiPlantes(IServeiApiPlantes):
    """
    Implementació de ServeiApiPlantes per la plataforma OpenDataBCN
    ...

    Attributes
    ----------
    url : str
        L'URL per accedir al conjunt de dades dessitjat
    recurs : str
        Identificador del conjunt de dades


    Methods
    -------
    descarregar() -> List[PlantaDTO]:
        Descarrega les plantes de la plataforma.\n
        Font: [Arbrat dels parcs de la ciutat de Barcelona](https://opendata-ajuntament.barcelona.cat/data/ca/dataset/arbrat-parcs)
        Llicencia: CC-BY-4.0
    """

    # def __init__(self, *args, **kwargs):
    #     super().__init__(
    #         acreditacio=Acreditacio(
    #             entitat=Entitat(
    #                 nom='OpenDataBCN | Arbrat dels parcs de la ciutat de Barcelona',
    #                 enllac="https://opendata-ajuntament.barcelona.cat/data/ca/dataset/arbrat-parcs"
    #             ),
    #             llicencia=LlicenciaFactory.creative_commons_by_4_0()
    #         )
    #     )
    #     self.url = kwargs.get('url')
    #     self.recurs = kwargs.get('recurs')
    def __init__(self, acreditacio: Acreditacio, **kwargs):
        super().__init__(acreditacio)
        self.url = kwargs.get('url')
        self.recurs = kwargs.get('recurs')

    def descarregar(self):

        def parseja_elements(elements: List[Dict]) -> List[PlantaDTO]:

            def parseja(element: Dict) -> PlantaDTO:
                dades_posicio = {
                    'coordenades': shapely.wkt.loads(element['geom']),
                    'espai_verd': treure_espais(element['espai_verd'])
                }

                def nom_o_llista_noms(nom_comu_item: str):
                    if nom_comu_item.count(';') == 0:
                        return treure_espais(nom_comu_item)
                    else:
                        return list(map(treure_espais, nom_comu_item.split(';')))

                nom_comu = {'ca': nom_o_llista_noms(element['nom_catala']),
                            'es': nom_o_llista_noms(element['nom_castella']),
                            'en': ""
                            }

                return PlantaDTO(
                    treure_espais(element['nom_cientific']),
                    nom_comu,
                    dades_posicio=dades_posicio
                )

            return list(map(parseja, elements))

        def generar_plantilla_peticio(id_recurs):
            plantes = Table(id_recurs)
            plantilla_sql = Query.select(
                plantes.cat_especie_id,
                fn.Trim(plantes.espai_verd, alias='espai_verd'),
                plantes.nom_districte,
                fn.Trim(plantes.nom_cientific, alias='nom_cientific'),
                plantes.nom_castella,
                plantes.nom_catala,
                fn.Min(plantes.geom, alias='geom')
            ).from_(plantes).groupby(
                plantes.cat_especie_id,
                plantes.espai_verd,
                plantes.nom_districte,
                plantes.nom_cientific,
                plantes.nom_castella,
                plantes.nom_catala
            ).orderby(
                plantes.cat_especie_id,
                plantes.espai_verd
            )

            return plantilla_sql


        url = f"{self.url}/data/api/action/datastore_search_sql"
        elements = []
        offset = 0
        complert = False
        resultats_per_consulta = 100
        peticio_sql = generar_plantilla_peticio(self.recurs)

        while not complert:

            print(f"Offset: {offset}")

            seguents_cent_plantes = {
                'sql': peticio_sql.limit(resultats_per_consulta).offset(offset).get_sql()
            }

            try:

                resposta = requests.get(
                    url, params=seguents_cent_plantes, timeout=self._TEMPS_ESPERA)
                resposta.encoding = 'latin-1'
                json_resposta = resposta.json()
                hi_ha_noves_dades = (resposta.status_code == 200) and \
                                    json_resposta['success'] and \
                                    (len(json_resposta["result"]["records"]) > 0)

                if hi_ha_noves_dades:
                    elements.extend(json_resposta["result"]["records"])
                    offset += resultats_per_consulta
                else:
                    complert = True

            except Exception as error_desconegut:
                complert = True
                raise error_desconegut
        return parseja_elements(elements)
