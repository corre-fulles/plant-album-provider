import os
from typing import List, Dict

import geopandas as gpd
import requests
from fiona.crs import from_epsg

from app.domain.entities import Acreditacio
from app.domain.entities import EspaiVerdDTO
from app.domain.interfaces import IServeiApiEspaisVerds
from app.utils import treure_espais


class OpenDataBcnServeiApiEspaisVerds(IServeiApiEspaisVerds):
    """
    Implementació de ServeiApiEspaisVerds per la plataforma OpenDataBCN

    This is the docstring for your function.
    -------------------------------
    ### Title 3 Heading



    Atributs
    ----
    url : str
        L'URL per accedir al conjunt de dades dessitjat
    dataset: str
        Identificador del del conjunt de dades
    recurs : str
        Identificador de la versió del conjunt de dades



    Methods
    -------
    descarregar() -> List[EspaiVerdDTO]:
        Descarrega les plantes de la plataforma.\n
        Font: [Espais verds públics més grans de 0,5 ha a la ciutat de Barcelona](https://opendata-ajuntament.barcelona.cat/data/ca/dataset/espais-verds-publics)
        Llicencia: CC-BY-4.0
    """

    # def __init__(self, *args, **kwargs):
    #     super().__init__(
    #         acreditacio=Acreditacio(
    #             entitat=Entitat(
    #                 nom='OpenDataBCN | Espais verds públics més grans de 0,5 ha a la ciutat de Barcelona',
    #                 enllac="https://opendata-ajuntament.barcelona.cat/data/ca/dataset/espais-verds-publics"
    #             ),
    #             llicencia=LlicenciaFactory.creative_commons_by_4_0()
    #         )
    #     )
    #     self.url = kwargs.get('url')
    #     self.dataset = kwargs.get('dataset')
    #     self.recurs = kwargs.get('recurs')

    def __init__(self, acreditacio: Acreditacio, **kwargs):
        super().__init__(acreditacio)
        print(acreditacio)
        print(kwargs)

        self.url = kwargs.get('url')
        self.recurs = kwargs.get('recurs')
        self.dataset = kwargs.get('dataset')
        

    def descarregar(self) -> List[EspaiVerdDTO]:
        def parseja_espaisverds(llista_dades_espais: List[Dict]):
            return [
                EspaiVerdDTO(
                    fila['nom'],
                    fila['districte'],
                    dades_posicio={'coordenades': fila['geometry']}
                ) for fila in llista_dades_espais
            ]
                
        def llegir_items(nom_arxiu_gkpg) -> List[Dict]:
            df_espais = gpd.read_file(nom_arxiu_gkpg,
                                  include_fileds=['nom', 'districte'],
                                  driver="GPKG",
                                  layer='pev_parcs_od',
                                  engine='fiona',
                                  crs=from_epsg(32631)
                                  )
            df_espais["districte"] = df_espais["districte"].values.astype(str)
            dades_espais = [{
            'nom': treure_espais(fila['nom']),
            'districte':  fila['districte'],
            'geometry': fila.get('geometry', None)
            } for _, fila in df_espais.iterrows()]
            return dades_espais
        
                
            
            

        url = f"{self.url}/data/dataset/{self.dataset}/resource/{self.recurs}/download/pev_parcs_od.gpkg"
        nom_arxiu_temporal = 'bcn.gpkg'
        resposta_obtenir_dataset = requests.get(
            url, timeout=self._TEMPS_ESPERA)
        with open(nom_arxiu_temporal, 'wb') as arxiu_temporal:
            arxiu_temporal.write(resposta_obtenir_dataset.content)
            
        dades_espais_verds = llegir_items(nom_arxiu_temporal)        
        os.remove(nom_arxiu_temporal)

        return parseja_espaisverds(dades_espais_verds)
