from typing import List

import requests
from app.domain.entities import EspaiVerdDTO
from app.domain.interfaces.serveis import IServeiApiEspaisVerds


class OpenStreetMapEspaisVerdsQuery:
    def __init__(self):
        self.output_format = None
        self.timeout = 0
        self.city = None
        self.min_area = 0

    def in_format(self, response_format: str):
        self.output_format = f"[out:{response_format}]"
        return self

    def with_timeout(self, request_timeout: int):
        self.timeout = f"[timeout:{request_timeout}]"
        return self

    def at_city(self, city_name: str):
        self.city = f"area[\"name\"~\"^{city_name}$\",i][admin_level=8]"
        return self
    def at_country(self, country_code:str):
        self.country =  f"area[\"ISO3166-1\"=\"{country_code}\"][admin_level=2]"

    def with_area_greater_than(self, smallest_area: int):
        self.min_area = smallest_area
        return self

    def __get_parks_and_gardens(self):
        area_greater_than_min = f"length() > {self.min_area}"

        parks_query = f"""wr["name"]["leisure"="park"](if: {area_greater_than_min})(area.searchArea)(area.country);"""

        gardens_query = f"""wr["name"]["leisure"="garden"](if: {area_greater_than_min})(area.searchArea)(area.country);"""

        return f"{parks_query}\n{gardens_query}"

    def build(self):
        query_text = f"""{self.output_format}{self.timeout};
        ({self.country})->.country;
        ({self.city})->.searchArea;
        ({self.__get_parks_and_gardens()});        
        out tags;"""
        self.output_format = None
        self.timeout = 0
        self.city = None
        self.min_area = 0
        return query_text


class EspaiVerdOpenStreetMapAdapter(IServeiApiEspaisVerds):

    def __init__(self, nom_ciutat: str, area_minima=0):
        self.nom_ciutat = nom_ciutat
        self.area_minima = area_minima

    def descarregar(self) -> List[EspaiVerdDTO]:
        peticio = OpenStreetMapEspaisVerdsQuery().at_city(self.nom_ciutat).with_area_greater_than(
            self.area_minima
        ).with_timeout(self._TEMPS_ESPERA).in_format('json').build()

        resposta = requests.get(
            'https://overpass-api.de/api/interpreter', params={'data': peticio})

        json_resposta = resposta.json()

        return [EspaiVerdDTO(nom=elem['tags']['name']) for elem in json_resposta['elements']]
