from functools import singledispatch

from app.domain.interfaces import IServeiApi, IServeiApiEspaisVerds, IServeiApiPlantes


class LocalitzadorApis:

    def __init__(self) -> None:
        self.apis = {
            'espais': {},
            'plantes': {}
        }

    def afegir(self, clau: str, valor: IServeiApi):
        @singledispatch
        def afegir_valor(nou_valor):
            raise NotImplementedError

        @afegir_valor.register
        def _(nou_valor: IServeiApiEspaisVerds):
            self.apis['espais'][clau] = nou_valor

        @afegir_valor.register
        def _(nou_valor: IServeiApiPlantes):
            self.apis['plantes'][clau] = nou_valor

        afegir_valor(valor)

    def trobar_servei_plantes(self, nom: str) -> IServeiApiPlantes:
        return self.apis['plantes'][nom]

    def trobar_servei_espais(self, nom: str) -> IServeiApiEspaisVerds:
        return self.apis['espais'][nom]
