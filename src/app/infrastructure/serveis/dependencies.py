from dependency_injector import providers, containers, errors

from app.config import AppConfigContainer
from app.domain.entities import Acreditacio, Entitat, Llicencia
from app.domain.errors import ErrorDependencia
from app.domain.interfaces import ServeiImportadorAlbums
from .api import OpenDataBcnServeiApiEspaisVerds, OpenDataBcnServeiApiPlantes
from .importadors import ServeiImportadorAlbumsBarcelona


class ApiEspaisVerdsContainer(containers.DeclarativeContainer):
    config = AppConfigContainer.config

    acreditacio_opendatabcn = providers.Singleton(
        Acreditacio,
        entitat=providers.Singleton(
            Entitat,
            nom=config.apis['espais verds'].opendatabcn.acreditacio.entitat.nom,
            enllac=config.apis['espais verds'].opendatabcn.acreditacio.entitat.enllac
        ),
        llicencia=Llicencia.creative_commons_attribution_v4())

    opendatabcn = providers.Factory(
        OpenDataBcnServeiApiEspaisVerds,
        acreditacio_opendatabcn,
        url=config.apis['espais verds'].opendatabcn.parametres.url,
        recurs=config.apis['espais verds'].opendatabcn.parametres.recurs,
        dataset=config.apis['espais verds'].opendatabcn.parametres.dataset
    )


class ApiPlantesContainer(containers.DeclarativeContainer):
    config = AppConfigContainer.config

    acreditacio_opendatabcn = providers.Singleton(
        Acreditacio,
        entitat=providers.Singleton(
            Entitat,
            nom=config.apis.plantes.opendatabcn.acreditacio.entitat.nom,
            enllac=config.apis.plantes.opendatabcn.acreditacio.entitat.enllac
        ),
        llicencia=Llicencia.creative_commons_attribution_v4())

    opendatabcn = providers.Factory(
        OpenDataBcnServeiApiPlantes,
        acreditacio_opendatabcn,
        url=config.apis.plantes.opendatabcn.parametres.url,
        recurs=config.apis.plantes.opendatabcn.parametres.recurs
    )


class Importadors(containers.DeclarativeContainer):
    config = AppConfigContainer.config

    barcelona = providers.Singleton(
        ServeiImportadorAlbumsBarcelona,
        api_espais_verds=ApiEspaisVerdsContainer.opendatabcn,
        api_plantes=ApiPlantesContainer.opendatabcn
    )


class ImportadorAlbumsSelector(containers.DeclarativeContainer):
    config = AppConfigContainer.config

    importador = providers.Selector(
        config.console.importer.pais,
        spain=providers.Selector(
            config.console.importer.ciutat,
            barcelona=Importadors.barcelona
        )
    )


def seleccionar_importador(pais, ciutat, importador_selector: ImportadorAlbumsSelector) -> ServeiImportadorAlbums:
    try:
        importador_selector.config.console.importer.from_dict({'pais': pais, 'ciutat': ciutat})
        return importador_selector.importador()
    except errors.Error:
        raise ErrorDependencia(f"No existeix cap importador d'àlbums per {ciutat}, {pais}")
    except Exception as error:
        raise error
