from functools import singledispatchmethod
from typing import List

from app.domain.entities import Album
from app.infrastructure.persistencia.database import MongoDBDriver, AlbumMongoDBDTO
from app.middleware.persistencia.repository import AbstractDBAlbumRepository


class MongoDBAlbumRepository(AbstractDBAlbumRepository):

    def __init__(self, driver: MongoDBDriver, pais:str=None, ciutat:str=None):
        self.driver = driver
        if pais and ciutat:
            self.driver.connectar(pais, ciutat)

    @singledispatchmethod
    def afegir(self, album, pais: str, ciutat: str):
        raise NotImplementedError

    @afegir.register
    def _(self, album: Album, pais: str, ciutat: str):
        self._activar(pais, ciutat)
        self.driver.afegir_document(AlbumMongoDBDTO.to_mongodb_document(album))

    @afegir.register
    def _(self, albums: list, pais: str, ciutat: str):
        self._activar(pais, ciutat)
        self.driver.afegir_documents(
            list(map(AlbumMongoDBDTO.to_mongodb_document, albums)))

    def _activar(self, pais: str, ciutat: str):
        self.driver.connectar(pais, ciutat)

    def amb_entrades(self, pais: str, ciutat: str) -> List[Album]:
        self._activar(pais, ciutat)
        albums_amb_entrades = {'$match': {'plantes.0': {'$exists': True}}}
        seleccionar_camps_album = {'$project': {
            'espai': 1, 'districte': 1, 'plantes': 1, 'fonts': 1, '_id': 0}}
        ordenar_per_districte_creixentment = {'$sort': {'districte': 1, 'espai': 1}}
        fases = [albums_amb_entrades, seleccionar_camps_album,
                 ordenar_per_districte_creixentment]
        cursor = self.driver.obtenir_agregat(fases)
        return list(map(AlbumMongoDBDTO.from_mongodb_document, cursor))

