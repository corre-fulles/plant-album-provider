from app.domain.entities import Album, Planta, EspaiVerd, NomComu, DTOManager
from app.domain.entities.credit import Acreditacio, Entitat, Llicencia


class MongoDBDTO(DTOManager):

    @classmethod
    def to_mongodb_document(cls, domain_object):
        raise NotImplementedError

    @classmethod
    def to_dto(cls, domain_object):
        return cls.to_mongodb_document(domain_object)

    @classmethod
    def from_mongodb_document(cls, document):
        raise NotImplementedError

    @classmethod
    def from_dto(cls, external_dto):
        return cls.from_mongodb_document(external_dto)


class PlantaMongoDBDTO(MongoDBDTO):

    @classmethod
    def to_mongodb_document(cls, domain_object: Planta):
        document = {
            'nom_cientific': domain_object.nom_cientific,
            'nom_comu': domain_object.nom_comu.traduccions
        }
        return document

    @classmethod
    def from_mongodb_document(cls, document: dict) -> Planta:
        return Planta(document['nom_cientific'], NomComu(document['nom_comu']))


class EspaiVerdMongoDBDTO(MongoDBDTO):

    @classmethod
    def to_mongodb_document(cls, domain_object: EspaiVerd):
        document = {
            'nom': domain_object.nom,
            'districte': domain_object.districte or None
        }
        return document

    @classmethod
    def from_mongodb_document(cls, document: dict) -> EspaiVerd:
        return EspaiVerd(document['nom'], document.get('districte', None))


class AcreditacioMongoDBDTO(MongoDBDTO):
    @classmethod
    def to_mongodb_document(cls, domain_object: Acreditacio):
        document = {
            'entitat': domain_object.entitat.toJSON(),
            'llicencia': domain_object.llicencia.toJSON()
        }

        return document

    @classmethod
    def from_mongodb_document(cls, document: dict) -> Acreditacio:
        return Acreditacio(
            entitat=Entitat(
                nom=document['entitat']['nom'], enllac=document['entitat']['url']),
            llicencia=Llicencia(
                nom=document['llicencia']['nom'],
                acronim=document['llicencia']['acronim'],
                enllac=document['llicencia']['url']
            )
        )


class AlbumMongoDBDTO(MongoDBDTO):

    @classmethod
    def to_mongodb_document(cls, domain_object: Album):
        document = {'espai': EspaiVerdMongoDBDTO.to_mongodb_document(domain_object.espai),
                    'plantes': [],
                    'fonts': {
                        'espais verds': AcreditacioMongoDBDTO.to_mongodb_document(
                            domain_object.acreditacions['espais verds']),
                        'plantes': AcreditacioMongoDBDTO.to_mongodb_document(
                            domain_object.acreditacions['plantes'])
                    }}
        if len(domain_object.plantes) > 0:
            document['plantes'] = list(
                map(PlantaMongoDBDTO.to_mongodb_document, domain_object.plantes))
        return document

    @classmethod
    def from_mongodb_document(cls, document: dict) -> Album:
        espai = EspaiVerdMongoDBDTO.from_mongodb_document(document['espai'])
        plantes = list(
            map(PlantaMongoDBDTO.from_mongodb_document, document['plantes']))
        fonts = {
            'espais verds': AcreditacioMongoDBDTO.from_mongodb_document(document['fonts']['espais verds']),
            'plantes': AcreditacioMongoDBDTO.from_mongodb_document(document['fonts']['plantes'])
        }
        return Album(espai, plantes, acreditacions=fonts)
