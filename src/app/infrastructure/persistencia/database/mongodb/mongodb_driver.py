from typing import Dict, List

from app.middleware.persistencia.database import DBDriver, DataBaseError
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.errors import ConnectionFailure


class MongoDBDriver(DBDriver):

    def __init__(self, host: str, port: int, db_auth=None, **kwargs):
        self._client = MongoClient(
            host, port, **kwargs)
        self._db = None
        self._nom_coleccio_activa = None

    def connectar(self, nom_bd: str, nom_coleccio: str):

        self._db = self._client.get_database(nom_bd)
        try:
            self._db.command('ping')
            self._nom_coleccio_activa = nom_coleccio
        except ConnectionFailure:
            self._db = None
            self._nom_coleccio_activa = None
            raise



    @property
    def esta_connectat(self):
        return self._db is not None and self._nom_coleccio_activa is not None

    @property
    def coleccio_activa(self) -> Collection:
        if self.esta_connectat:
            return self._db.get_collection(self._nom_coleccio_activa)
        else:
            raise DataBaseError("No s'ha fet cap connexio")

    def afegir_documents(self, documents: List[Dict]):
        self.coleccio_activa.insert_many(documents)

    def afegir_document(self, document: Dict):
        self.coleccio_activa.insert_one(document)

    def obtenir_agregat(self, condicions: List):
        return self.coleccio_activa.aggregate(pipeline=condicions, maxTimeMS=5000)
