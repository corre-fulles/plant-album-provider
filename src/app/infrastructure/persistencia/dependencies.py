import os
from distutils.util import strtobool

from dependency_injector import providers, containers

from app.config import AppConfigContainer
from .database import MongoDBDriver
from .repository import MongoDBAlbumRepository


class DatabaseDriverContainer(containers.DeclarativeContainer):
    config = AppConfigContainer.config
    config.databases.mongodb.host.from_env('DB_MONGODB_HOST', required=True)
    config.databases.mongodb.port.from_env('DB_MONGODB_PORT', required=True, as_=int)
    config.databases.mongodb.is_local.from_env('DB_MONGODB_IS_LOCAL', required=True, as_=(lambda x: strtobool(x)))
    # Carreguem dependències per MongoDB

    ruta_certificat_mongodb_remot = providers.Callable(
        os.path.join,
        os.path.dirname(__file__),
        "certs",
        config.certificats.mongodb or "res.pem"
    )

    mongodb = providers.Singleton(
        MongoDBDriver,
        host=config.databases.mongodb.host,
        port=config.databases.mongodb.port,
        tls=not config.databases.mongodb.is_local(),
        tlsCertificateKeyFile=ruta_certificat_mongodb_remot if not config.databases.mongodb.is_local() else None
    )




class AlbumRepositoryContainer(containers.DeclarativeContainer):
    config = DatabaseDriverContainer.config

    repositori = providers.Singleton(
        MongoDBAlbumRepository,
        driver=DatabaseDriverContainer.mongodb
    )

    # ruta_certificat_remot = providers.Callable(
    #     lambda nom_certificat: os.path.join(os.path.dirname(__file__), "certs", nom_certificat),
    #     config.certificats.mongodb
    # )
    #
    # driver = providers.Singleton(
    #     MongoDBDriver,
    #     host=config.host,
    #     port=config.port,
    #     tls=not config.is_local,
    #     tlsCertificateKeyFile=ruta_certificat_remot() if not config.is_local else None
    # )


    
    
