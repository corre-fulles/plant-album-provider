import os

from dependency_injector import containers, providers


class AppConfigContainer(containers.DeclarativeContainer):
    """
        Contenidor de tots els valors de configuració del programa:

        # config.app.environment[str] entorn d'execució  = 'dev' - Desenvolupament ; 'prod' - Producció
        # config.web.agent[str] Framework per executar l'aplicació web = 'flask' - Flask ; 'falcon' - Falcon

        # config.console.importer.pais[str] País de la ciutat on importar àlbums
        # config.console.importer.ciutat[str] Ciutat on importar àlbums

        # config.certificats.<nom_db> certificat per connexió remota a la base de dades
        # config.databases.<nom_db>.<atribut> Atributs necessaris per connectar a una base de dades (host, port, credencials, etc)

    	# APIs (tipus = 'espais verds' (config.apis['espais verds']) || 'plantes' (config.apis.plantes)
        # config.apis.<tipus>.<nom_api>.parametres[Any] Paràmetres de __init__ de  la implementació
           de ServeiApiEspaisverds o ServeiApiPlantes  per <nom_api>
        # config.apis.<tipus>.<nom_api>.acreditacio.entitat.nom[str]
        # config.apis.<tipus>.<nom_api>.acreditacio.entitat.url[str]
        # config.apis.<tipus>.<nom_api>.acreditacio.llicencia.nom[str] (OPCIONAL*)
        # config.apis.<tipus>.<nom_api>.acreditacio.llicencia.acronim[str] (OPCIONAL*)
        # config.apis.<tipus>.<nom_api>.acreditacio.llicencia.url[str] (OPCIONAL*)

        *Nota per desenvolupadors: si les dades de molts valors de <nom_api> estan sota una llicéncia oficial,
        es recomana editar la classe Llicencia a app.domain.entities.credits.py
        afegint un @classmethod per aquell tipus de llicéncia.
        Es pot seguir l'exemple següent per la Creative Commons Attribution 4.0 International license:

    	@classmethod
        def creative_commons_attribution_v4(cls):
            return cls(
                nom="Creative Commons Attribution 4.0 International license",
                acronim="CC-BY 4.0",
                enllac="https://creativecommons.org/licenses/by/4.0/"
            )

        """
    config = providers.Configuration(
        name="configuracio arrel",
        json_files=[
            os.path.join(os.path.dirname(__file__), 'apis.json'),
            os.path.join(os.path.dirname(__file__), 'certificats.json')
        ]
    )
    config.app.environment.from_env('ENVIRONMENT', default='dev')





# llicencia_cc_v4 = providers.Object(Llicencia.creative_commons_by_4_0())
#
# opendata_bcn_plantes = providers.Factory(
#     OpenDataBcnServeiApiPlantes,
#     acreditacio=providers.Factory(
#         Acreditacio,
#         entitat=providers.Singleton(
#             Entitat,
#             nom=config.apis.plantes.opendatabcn.acreditacio.entitat.nom,
#             enllac=config.apis.plantes.opendatabcn.acreditacio.entitat.enllac
#         ),
#         llicencia=llicencia_cc_v4),
#     url=config.apis.plantes.opendatabcn.parametres.url,
#     recurs=config.apis.plantes.opendatabcn.parametres.recurs,
# )
#
# opendata_bcn_espais = providers.Factory(
#     OpenDataBcnServeiApiEspaisVerds,
#     providers.Factory(
#         Acreditacio,
#         entitat=providers.Singleton(
#             Entitat,
#             nom=config.apis['espais verds'].opendatabcn.acreditacio.entitat.nom,
#             enllac=config.apis['espais verds'].opendatabcn.acreditacio.entitat.enllac
#         ),
#         llicencia=llicencia_cc_v4),
#     url=config.apis['espais verds'].opendatabcn.parametres.url,
#     recurs=config.apis['espais verds'].opendatabcn.parametres.recurs,
#     dataset=config.apis['espais verds'].opendatabcn.parametres.dataset
#
# )
