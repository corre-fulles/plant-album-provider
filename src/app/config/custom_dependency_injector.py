from app.infrastructure.serveis import LocalitzadorApis


class ImportadorAlbumsDependencyInjector:
    '''
    Classe encarregada de injectar les dependències
    '''

    def __init__(self, localitzador: LocalitzadorApis) -> None:
        self.localitzador = localitzador

    def registrar_depencencia(self, api_espais: str, api_plantes: str):
        '''
        Us: sigui di una instància de ImportadorAlbumsDependencyInjector :

        @di.registrar_depencencia(api_espais="X", api_plantes="Y")
        class ImplementacioDeImportadorAlbums(ImportadorAlbums):
        //  codi

        X i Y han de estar presents a l'arxiu apis.json:
        {
            "apis": {
                ...,
                "X" : { ... },
                ...,
                "Y" : { ... },
                ...,
            }

        }        

        '''

        def decorator(cls):
            def constructor_wrapper(*args, **kwargs):
                return cls(
                    api_espais_verds=self.localitzador.trobar_servei_espais(
                        api_espais),
                    api_plantes=self.localitzador.trobar_servei_plantes(
                        api_plantes),
                    *args, **kwargs)

            return constructor_wrapper

        return decorator
