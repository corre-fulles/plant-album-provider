from .dependencies import AppConfigContainer

'''   
def carregar_configuracio(nom_arxiu: str) -> dict:
    ruta_complerta_arxiu = os.path.join(os.path.dirname(os.path.abspath(__file__)), nom_arxiu)
    return json.load(open(ruta_complerta_arxiu, mode='r', encoding='utf-8'))


def afegir_servei_api(nom_servei: str, tipus: str, servei: Type[ServeiApi], configuracions_apis: dict, loacalitzador: LocalitzadorApis):
    parametres_api: dict = configuracions_apis[nom_servei]['serveis'][tipus]
    loacalitzador.afegir(nom_servei, servei(**parametres_api))


def ruta_certificat_remot():
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), 'certs', 'mongodb-remote.pem')


localitzador_apis = LocalitzadorApis()

dades_apis = carregar_configuracio('apis.json')
dades_apis = dades_apis['apis']

afegir_servei_api('opendatabcn', 'espais verds',
                  OpenDataBcnServeiApiEspaisVerds, dades_apis, localitzador_apis)
afegir_servei_api('opendatabcn', 'plantes',
                  OpenDataBcnServeiApiPlantes, dades_apis, localitzador_apis)


localitzador_importadors = ImportadorFactory()
localitzador_importadors.afegir_serveis({
    'spain': {
        'barcelona': ImportadorAlbumsBarcelona(
            api_espais_verds=localitzador_apis.trobar_servei_espais(
                'opendatabcn'),
            api_plantes=localitzador_apis.trobar_servei_plantes('opendatabcn')
        )
    }
})
'''
