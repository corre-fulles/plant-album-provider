import os
import re


def treure_espais(text: str):
    return text.strip()

class Validador:
    PATRO_URL = r'^https?://[^\s/$.?#].[^\s]*$'

    @classmethod
    def text_es_url(cls, text: str) -> bool:
        return bool(re.match(cls.PATRO_URL, text))

    @classmethod
    def text_es_no_nul(cls, text: str) -> bool:
        return bool(text)





def ruta_completa_arxiu(nom_modul: str = __file__, nom_arxiu: str = ""):
    return os.path.join(os.path.dirname(os.path.abspath(nom_modul)), nom_arxiu)


class BaseLogger:

    @staticmethod
    def info(text: str):
        print(f"[INFO]: {text}")

    @staticmethod
    def error(text: str):
        print(f"[ERROR]: {text}")
