from abc import ABC, abstractmethod


class PeticioCasUs(ABC):
    def __init__(self, *args, **kwargs):
        raise NotImplementedError


class RespostaCasUs(ABC):

    def __init__(self, *args, **kwargs):
        raise NotImplementedError


class ICasUs(ABC):

    @abstractmethod
    def executar(self, peticio: PeticioCasUs) -> RespostaCasUs:
        raise NotImplementedError
