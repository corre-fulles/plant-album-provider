from typing import List

from app.domain.entities import Album
from app.domain.interfaces import IAlbumRepository

from .cas_us import ICasUs, PeticioCasUs, RespostaCasUs


class PeticioObtenirAlbumsDeCiutat(PeticioCasUs):

    def __init__(self, pais: str, ciutat: str):
        self.ciutat = ciutat
        self.pais = pais


class RespostaObtenirAlbumsDeCiutat(RespostaCasUs):

    def __init__(self, albums: List[Album]):
        self.albums = albums


class ObtenirAlbumsDeCiutat(ICasUs):
    """
    Com a usuari, vull obtenir una llista de tots els àlbums d'una ciutat
    """

    def __init__(self, repositori: IAlbumRepository):
        self.repositori = repositori

    def executar(self, peticio: PeticioObtenirAlbumsDeCiutat) -> RespostaObtenirAlbumsDeCiutat:
        llista_albums = self.repositori.amb_entrades(peticio.pais, peticio.ciutat)
        return RespostaObtenirAlbumsDeCiutat(albums=llista_albums)
