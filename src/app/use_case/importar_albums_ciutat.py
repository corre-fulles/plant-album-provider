from typing import List

from app.domain.entities import Album
from app.domain.interfaces import ServeiImportadorAlbums

from .cas_us import ICasUs, RespostaCasUs


class RespostaImportarAlbums(RespostaCasUs):

    def __init__(self, albums: List[Album]):
        self.albums = albums


class ImportarAlbums(ICasUs):

    def __init__(self, importador: ServeiImportadorAlbums) -> None:
        self.importador = importador

    def executar(self) -> RespostaImportarAlbums:
        return RespostaImportarAlbums(self.importador.importar_albums())
