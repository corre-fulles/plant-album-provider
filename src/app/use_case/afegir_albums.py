from typing import List

from app.domain.entities import Album
from app.domain.interfaces import IAlbumRepository

from .cas_us import ICasUs, PeticioCasUs, RespostaCasUs


class PeticioAfegirAlbums(PeticioCasUs):
    def __init__(self, pais: str, ciutat: str, albums: List[Album]):
        self.pais = pais
        self.ciutat = ciutat
        self.albums = albums


class RespostaAfegirAlbums(RespostaCasUs):
    def __init__(self, resultat: dict):
        self.resultat = resultat


class AfegirAlbums(ICasUs):

    def __init__(self, repositori: IAlbumRepository) -> None:
        self.repositori = repositori

    def executar(self, peticio: PeticioAfegirAlbums) -> RespostaAfegirAlbums:
        self.repositori.afegir(peticio.albums, peticio.pais, peticio.ciutat)
        return RespostaAfegirAlbums({
            'status': 'OK'
        })
