from abc import ABC, abstractmethod
from typing import List

from app.domain.entities import Album


class IAlbumRepository(ABC):

    @abstractmethod
    def afegir(self, album, pais, ciutat):
        raise NotImplementedError

    @abstractmethod
    def amb_entrades(self, pais, ciutat) -> List[Album]:
        raise NotImplementedError
