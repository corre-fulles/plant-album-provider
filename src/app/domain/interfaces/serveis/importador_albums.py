from abc import ABC, abstractmethod
from typing import List

from app.domain.entities import Album, EspaiVerdDTO, PlantaDTO

from .servei_api import IServeiApiEspaisVerds, IServeiApiPlantes


class ServeiImportadorAlbums(ABC):
    """
    Classe abstracta del servei importador d'àlbums
    """
    def __init__(self,
                 api_espais_verds: IServeiApiEspaisVerds,
                 api_plantes: IServeiApiPlantes
                 ):
        self._api_espais = api_espais_verds
        self._api_plantes = api_plantes

    @abstractmethod
    def _crear_albums(self, espais: List[EspaiVerdDTO], plantes: List[PlantaDTO]) -> List[Album]:
        """
        Crea una llista d'Albums a partir dels DTO d'Espais Verds i de Plantes.
        """
        pass

    def importar_albums(self) -> List[Album]:
        """
        Retorna una llista d'Albums a partir de la informació obtinguda de l'API d'espais verds i l'API de plantes.
        """
        espais = self._api_espais.descarregar()
        plantes = self._api_plantes.descarregar()
        return self._crear_albums(espais, plantes)
