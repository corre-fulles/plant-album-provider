from abc import ABC, abstractmethod
from typing import List

from app.domain.entities import Acreditacio, EspaiVerdDTO, PlantaDTO


class IServeiApi(ABC):
    """
    Classe abstracta del servei d'API.
    """
    # temps en segons d'espera de les solicituds a serveis externs
    _TEMPS_ESPERA = 10

    def __init__(self, acreditacio: Acreditacio, *args, **kwargs):
        self.acreditacio = acreditacio

    @abstractmethod
    def descarregar(self):
        raise NotImplementedError


class IServeiApiEspaisVerds(IServeiApi):
    """
    Classe abstracta per obtenir dades d'espais verds
    """

    @abstractmethod
    def descarregar(self) -> List[EspaiVerdDTO]:
        """
        Descarrega informació d'espais verds d'una API

        :return: Una llista de DTOs d'EspaiVerd.
        """
        raise NotImplementedError


class IServeiApiPlantes(IServeiApi):
    """
    Classe abstracta per obtenir dades de plantes
    """

    @abstractmethod
    def descarregar(self) -> List[PlantaDTO]:
        """
        Descarrega informació de plantes d'una API

        :return: Una llista de DTOs de Planta.
        """
        raise NotImplementedError
