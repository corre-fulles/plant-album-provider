from typing import Dict, List, Optional

from .credit import Acreditacio
from .espai_verd import EspaiVerd
from .planta import Planta

Plantes = List[Planta]


class Album(object):
    def __init__(self, espai: EspaiVerd, plantes: Optional[Plantes] = None,
                 acreditacions: Optional[Dict[str, Acreditacio]] = None):
        self.espai = espai
        self.plantes = plantes or []
        self.acreditacions = acreditacions or {}

    def acreditar(self, atribut: str, acreditacio: Acreditacio):
        self.acreditacions[atribut] = acreditacio

    def toJSON(self):
        return {
            'espai': self.espai.toJSON(),
            'plantes': list(map(lambda p: p.toJSON(), self.plantes)),
            'acreditacions': {
                'espais verds': self.acreditacions['espais verds'].toJSON(),
                'plantes': self.acreditacions['plantes'].toJSON()
            }
        }
