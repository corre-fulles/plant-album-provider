from abc import ABC, abstractmethod
from typing import List, Dict, Union, Optional


class PlantaDTO:
    def __init__(self,
                 nom_cientific: str,
                 nom_comu: Optional[Dict[str, Union[str, List[str]]]],
                 **parametres_opcionals
                 ):
        self.nom_cientific = nom_cientific
        self.nom_comu = nom_comu
        if parametres_opcionals:
            for nom_atribut, valor in parametres_opcionals.items():
                self.__setattr__(nom_atribut, valor)


class EspaiVerdDTO:

    def __init__(self, nom: str, districte: Optional[str] = None, **parametres_opcionals):
        '''
        Constructor del DTO d'un espai verd
        :param nom : Nom de l'espai verd
        :param districte : (Opcional) Nom del districte
        :param parametres_opcionals: (Opcional) Diccionari amb altres valors que es vulgui assignar al DTO.
        En cap cas els atributs generats per **parametres_opcionals** es conservaran en cas de conversió de DTO a entitat
        de domini @EspaiVerd.
        '''
        self.nom = nom
        self.districte = districte
        if parametres_opcionals:
            for nom_atribut, valor in parametres_opcionals.items():
                self.__setattr__(nom_atribut, valor)


class DTOManager(ABC):

    @classmethod
    @abstractmethod
    def to_dto(cls, domain_object):
        pass

    @classmethod
    @abstractmethod
    def from_dto(cls, external_dto):
        pass
