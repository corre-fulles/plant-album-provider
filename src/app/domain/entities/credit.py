from typing import Callable, Optional

from app.domain.errors import ErrorDomini


class Llicencia:
    """
        Representa una llicència a la qual les dades d'un :class:`Album` estan sotmeses.
    """

    def __init__(self, nom: str, acronim: str, enllac: str):
        """
        :param nom: El nom de la llicència
        :param acronim: L'acrònim que identifica la llicència
        :param enllac: L'URL amb informació sobre la llicència
        """
        if not bool(enllac):
            raise ErrorDomini("'enllac' no pot ser nul ni un valor buit")
        self.nom = nom
        self.acronim = acronim
        self.enllac = enllac

    def toJSON(self):
        return {
            'nom': self.nom,
            'acronim': self.acronim,
            'url': self.enllac
        }

    @classmethod
    def creative_commons_attribution_v4(cls):
        """
        Retorna una :class:`Llicencia` per la
        `Creative Commons Attribution 4.0 International license <https://creativecommons.org/licenses/by/4.0/>`_.
        """
        return cls(
            nom="Creative Commons Attribution 4.0 International license",
            acronim="CC-BY 4.0",
            enllac="https://creativecommons.org/licenses/by/4.0/"
        )


class Entitat:
    """
    Representa una font de la qual les dades d'un :class:`Album` són originàries.
    """

    def __init__(self, nom: str, enllac: str):
        if not bool(nom):
            raise ErrorDomini("'nom' no pot ser nul ni un valor buit")
        if not bool(enllac):
            raise ErrorDomini("'enllac' no pot ser nul ni un valor buit")
        self.nom = nom
        self.enllac = enllac

    def toJSON(self):
        return {
            'nom': self.nom,
            'url': self.enllac
        }


class Acreditacio:
    """
    Element que representa l'acció d'acreditar dades a una entitat sota una llicència.
    """

    def __init__(self, entitat: Entitat, llicencia: Llicencia,
                 funcio_acreditacio: Optional[Callable[[Entitat, Llicencia], str]] = None):
        """Constructor d':class:`Acreditació`.

        :param entitat: :class:`Entitat` de l'acreditació.
        :param llicencia: :class:`Llicencia` sota la que es fa l'acreditació.
        :param funcio_acreditacio: :class:`Optional[Callable[[Entitat, Llicencia], str]]` | None : Si no és None,
        funció que modifica el text de l'acreditació quan es representa com a string. Per defecte, el text
        d'una acreditacio es mostra així: "entitat.nom (llicencia.acronim)".
        """
        self.entitat = entitat
        self.llicencia = llicencia
        self.funcio_acreditacio = funcio_acreditacio

    @property
    def text_per_defecte(self):
        return f"{self.entitat.nom} ({self.llicencia.acronim})"

    def __str__(self):
        if self.funcio_acreditacio:
            return self.funcio_acreditacio(self.entitat, self.llicencia)
        else:
            return self.text_per_defecte

    def toJSON(self):
        return {
            'entitat': self.entitat.toJSON(),
            'llicencia': self.llicencia.toJSON()
        }


