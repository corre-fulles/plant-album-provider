from typing import Optional

from ..errors import ErrorDomini


class EspaiVerd(object):
    def __init__(self, nom: str, districte: Optional[str] = None):
        if not bool(nom):
            raise ErrorDomini("El nom no pot ser nul o un valor buit")
        self.nom = nom
        self.districte = districte

    def toJSON(self):
        return {'nom': self.nom, 'districte': self.districte}
