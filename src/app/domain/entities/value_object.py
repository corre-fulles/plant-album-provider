import json
from abc import ABC, abstractmethod


class ValueObject(ABC):

    @abstractmethod
    def value(self, *args, **kwargs):
        raise NotImplementedError

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__)
