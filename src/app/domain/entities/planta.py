from typing import Dict, List, Union

from .value_object import ValueObject
from ..errors import ErrorDomini

TraduccionsNom = Dict[str, Union[str, List[str]]]


class NomComu(ValueObject):

    def __init__(self, noms: TraduccionsNom):
        self.traduccions = noms

    def value(self, idioma: str):
        return self.traduccions.get(idioma, "")

    def toJSON(self) -> Dict:
        return self.traduccions


class Planta:
    def __init__(self, nom_cientific: str, nom_comu: NomComu):
        if not bool(nom_cientific):
            raise ErrorDomini("El nom científic no pot ser nul ni un valor buit")
        self.nom_cientific = nom_cientific
        self.nom_comu = nom_comu

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Planta):
            return self.nom_cientific.lower().strip() == other.nom_cientific.lower().strip()
        return False

    def toJSON(self) -> Dict:
        return {
            'nom_cientific': self.nom_cientific,
            'nom_comu': self.nom_comu.toJSON()
        }
