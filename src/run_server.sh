#!/bin/sh
app_port=$1
final_status=0
if echo "$app_port" | grep -Eq '^[0-9]+$'; then
    ip_address=$(ip -4 addr show eth0 | awk '/inet /{print $2}' | cut -d "/" -f 1)
    python manage.py serve --host "$ip_address" --port $app_port --use-ssl

else
    echo "Error. El port ha de ser un nombre"
    final_status=1
fi
exit $final_status

